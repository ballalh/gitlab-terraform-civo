output "env-dynamic-url" {
  value = "https://${civo_kubernetes_cluster.primary.api_endpoint}"
}
